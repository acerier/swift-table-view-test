//
//  AJCTableEntries.swift
//  Swift Table View Test
//
//  Created by Cognitive Puppy LLC on 12/6/14.
//  Copyright (c) 2014 Cognitive Puppy LLC. All rights reserved.
//

import Foundation

class TableEntries {
    
    var tableViewCellText : [String] = []
    
    init() {} // by defalt, the array is empty
    
    init( fromArrayOfStrings stringArray: [String] ) {
        tableViewCellText = stringArray
    }
    
    var count : Int { // read-only computed property
        get {
            return tableViewCellText.count
        }
    }
    
    func isValidIndex(index: Int) -> Bool {
        return index >= 0 && index <= self.count
    }
    
    subscript(index: Int) -> String? {
        get {
            assert(isValidIndex(index), "Index out of range")
            return tableViewCellText[index]
        }
        set {
            assert(isValidIndex(index), "Index out of range")
            tableViewCellText[index] = newValue!
        }
    }
    
    func append( cellString: String ) {
        tableViewCellText.append(cellString)
    }
    
}
