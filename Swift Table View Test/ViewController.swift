//
//  ViewController.swift
//  Swift Table View Test
//
//  Created by Andrew Cerier on 12/3/14.
//  Copyright (c) 2014 Cognitive Puppy LLC. All rights reserved.
//

import UIKit



class ViewController: UITableViewController {
    
    let cellEntry = TableEntries(fromArrayOfStrings: [ "Apple", "Orange", "Banana", "Lemon", "Lime", "Cherry" ])

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // Hide the Status Bar so it doesn't overlap the table view
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellEntry.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CellOne", forIndexPath: indexPath) as UITableViewCell
        
        if let label = cell.textLabel {
            
            label.text = cellEntry[indexPath.row]

        }
        else {
            println("UITableViewCell's UITextLabel is nil")
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("User selected table row \(cellEntry[indexPath.row])")
    }

}

